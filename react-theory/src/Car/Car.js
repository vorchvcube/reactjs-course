import React, {Component} from 'react';
/*import Radium from 'radium';*/
import classes from './Car.scss'

class Car extends Component {

    /*componentWillReceiveProps(nextProps, nextContext) {
        console.log('Car componentWillReceiveProps', nextProps)
    }

    shouldComponentUpdate(nextProps, nextState, nextContext) {
        console.log('Car shouldComponentUpdate', nextProps, nextState)
        return true;
    }

    componentWillUpdate(nextProps, nextState, nextContext) {
        console.log('Car componentWillUpdate', nextProps, nextState)
    }

    componentDidUpdate(prevProps, prevState, snapshot) {
        console.log('Car componentDidUpdate', prevProps, prevState)
    }*/

    render() {

        console.log('car render');

        if (Math.random() > 0.7) {
            throw new Error('error')
        }

        const style = {
            border: '1px solid #ccc',
            marginBottom: '10px',
            display: 'block',
            padding: '10px',
            ':hover': {
                boxShadow: '0 4px 4px'
            }
        };


        return (
            <div className={classes.Car} style={style}>
            <h3>Car name: { this.props.name }</h3>
            <p>
                Year:  <strong> { this.props.year } </strong>
            </p>
            <input type="text" onChange={this.props.onChangeName} value={this.props.name}/>
            <button onClick={this.props.onDelete}> Delete </button>
        </div>
        )
    }
}

export default Car;


