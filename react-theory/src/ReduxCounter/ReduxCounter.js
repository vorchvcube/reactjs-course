import React, { Component } from 'react'

import { connect } from 'react-redux'


class ReduxCounter extends Component {

    render() {
        return (
          <div style={{ padding: 20, border: '1px solid #ccc'}}>
              <h1> Counter { this.props.counter } </h1>
              <hr/>
              <div>
                  <button>
                      add
                  </button>
                  <button>
                      Sub
                  </button>
              </div>
          </div>
        )
    }
}

function mapStateToProps(state) {
    console.log('app', state);
    return {
        counter2: state.counter2
    }
}


export default connect(mapStateToProps)(ReduxCounter)
