 import  React from 'react';
import './App.scss';
import Car from './Car/Car';
 import Counter from "./Counter/Counter";
 import ReduxTheory from "./Redux/ReduxTheory";
 import ErrorBoundry from './ErrorBundary/ErrorBundary'



class App extends  React.Component {
    constructor(props) {
        console.log('app constructor');
        super(props);
        this.state  = {
            cars: [
                {
                    name: 'Ford',
                    year: 2018
                },
                /*{
                    name: 'Kia RIO',
                    year: 2011
                },
                {
                    name: 'Kia RIO',
                    year: 2011
                },
                {
                    name: 'Kia RIO',
                    year: 2011
                }*/
            ],
            pageTitle: 'React Component',
            showCars: false
        };
    }


    onChangeName ( name, index ) {
        let car = this.state.cars[index];
        car.name = name;
        let cars = [...this.state.cars];
        cars[index] = car;
        this.setState({cars})
    }
    onDelete ( index ) {
        const cars  = this.state.cars.concat();
        cars.splice(index, 1);
        this.setState({cars})
    }

    toggleCarsHandler = (  ) => {
        this.setState( {
            showCars: !this.state.showCars
        })
    };
/*

    componentWillMount() {
        console.log('App componentWillMount');
    }

    componentDidMount() {
        console.log('App componentDidMount');
    }
*/

    render () {
        console.log('App render');
      const divStyle = {
          textAlign: 'center'
      };

      let cars = null;

      if ( this.state.showCars ) {
        cars = this.state.cars.map(( car, index)=> {
            return (
                <ErrorBoundry key={index}>
                    <Car
                        key={index}
                        name={car.name}
                        year = {car.year}
                        onDelete={() => this.onDelete(index)}
                        onChangeName={event => this.onChangeName(event.target.value, index)}/>
                </ErrorBoundry>

            )
        })
      }

      return (
          <div className="App" style={divStyle}>

              {/*<h1> { this.state.pageTitle } </h1>*/}
              <h1>{this.props.title}</h1>
              <div>
                  <Counter />
              </div>
              <button className="AppButton" onClick={this.toggleCarsHandler}> Toggle cars </button>
              { cars }


              <ReduxTheory>

              </ReduxTheory>

          </div>

      );
  }

}

export default App;
