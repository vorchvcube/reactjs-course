import React, {Component} from 'react';
import { connect } from 'react-redux'

import classes from './ReduxTheory.scss'
import ReduxCounter from '../ReduxCounter/ReduxCounter'

class ReduxTheory extends Component {

   /* state = {
        counter: 0
    };
*/
    updateCounter(value) {
       /* this.setState({
            counter: this.state.counter + value
        })*/
    }

    render() {
        console.log('App', this.props);
        return (
            <div className={classes.Redux}>
                <h1> Счетчик <strong> { this.props.counter } </strong> </h1>

                <hr/>

                <div className={classes.Action}>
                    <button onClick={this.props.onAdd}> Добавить </button>
                    <button onClick={this.props.onSub}> Вычесть 1 </button>
                    <button onClick={()=> {this.props.onValue(10)}}> Прибавить 10 </button>
                </div>

                <ReduxCounter>

                </ReduxCounter>
            </div>
        )
    }
}

function mapStateToProps(state) {
    console.log('app', state);
    return {
        counter1: state.counter1,
        counter2: state.counter2
    }
}

function mapDispatchToProps(dispatch) {
    return {
        onAdd: ()=> dispatch({type: 'ADD'}),
        onSub: ()=> dispatch({type: 'SUB'}),
        onValue: value => dispatch({type: 'VALUE', value: value})
    }

}

export default connect(mapStateToProps, mapDispatchToProps)(ReduxTheory);


