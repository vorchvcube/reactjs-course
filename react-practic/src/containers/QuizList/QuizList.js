import React, { Component } from  'react'
import { NavLink } from 'react-router-dom'
import axios from 'axios'

import './QuizList.scss'
import Loader from '../../components/UI/Loader/Loader'

class QuizList extends Component {

    state = {
        quizList: [],
        loader: false
    };

    renderQuizes () {
        return this.state.quizList.map((quize, index)=> {
            return (
                <li key={index}>
                    <NavLink to={'/quiz/' + quize.kod}>
                        {quize.addr}
                    </NavLink>
                </li>
            )
        })
    }

    componentDidMount() {
        this.setState({
            loader: true
        });
        axios.get(`http://api.uk-kpd.ru/api/ObmenHomes`)
            .then(result => {
                this.setState({
                    quizList: result.data,
                    loader: false
                });
            })
            .then( results => {
                axios.get(`http://api.uk-kpd.ru/api/ObmenHomes`)
                    .then(res=> console.log(res))
            })
            .catch(err => {
                console.error(err)
            })
    }

    render() {
        return (
            <div className='QuizList'>

                <div>
                    <h1>Список тестов</h1>
                    {
                        this.state.loader ? <Loader/> :
                            <ul>
                                { this.renderQuizes() }
                            </ul>
                    }

                </div>
            </div>
        )
    }
}
export default QuizList;

