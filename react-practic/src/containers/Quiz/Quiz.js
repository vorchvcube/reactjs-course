import React, {Component} from  'react'
import './Quiz.scss'
import ActiveQuiz from '../../components/ActiveQuiz/ActiveQuiz'
import FinishedQuiz from '../../components/FinishedQuiz/FinishedQuiz'

class Quiz extends Component {
    state = {
        results: {},
        isFinished: false,
        activeQuestion: 0,
        answerState: null,
        quiz: [
            {
                question: 'Какого цвета небо?',
                id: 1,
                rightAnswerId: 2,
                answers: [
                    {
                        id: 1,
                        text: 'Черный'
                    },
                    {
                        id: 2,
                        text: 'Синий'
                    },
                    {
                        id: 3,
                        text: 'Красный'
                    },
                    {
                        id: 4,
                        text: 'Зеленый'
                    },
                ]
            },
            {
                question: 'в каком году основали Санкт-Петербург?',
                rightAnswerId: 4,
                id: 2,
                answers: [
                    {
                        id: 1,
                        text: '1700'
                    },
                    {
                        id: 2,
                        text: '1701'
                    },
                    {
                        id: 3,
                        text: '1702'
                    },
                    {
                        id: 4,
                        text: '1703'
                    },
                ]
            }
        ]
    };

    onAnswerClickHandler = (answerId) => {

        if ( this.state.answerState ) {
            const key = Object.keys(this.state.answerState)[0];
            if ( this.state.answerState[key] === 'success' ) {
                return
            }

        }

        console.log('Answer ID:', answerId);
        const question = this.state.quiz[this.state.activeQuestion];

        const results = this.state.results;

        if ( question.rightAnswerId === answerId ) {

            if ( !results[question.id] ) {
                results[question.id] = 'success'
            }


            this.setState({
                answerState: {[question.id]: 'success'},
                results
            });

            if ( this.isQuizFinished() ) {
                this.setState({
                    isFinished: true
                })
            } else {
                this.setState({
                    activeQuestion: this.state.activeQuestion + 1,
                    answerState: null
                });
            }

            /*const timeOut = window.setTimeout(()=> {

                window.clearTimeout(timeOut)
            }, 1000);*/
        } else {
            results[question.id] = 'error';
            this.setState({
                answerState: {[answerId]: 'error'},
                results
            });
        }
    };

    isQuizFinished () {
        return this.state.activeQuestion + 1 === this.state.quiz.length;
    }

    retryHandler = () => {
        this.setState({
            activeQuestion: 0,
            answerState: null,
            isFinished: false,
            results: {}
        });
    };

    componentDidMount() {
        console.log('quize id', this.props.match.params.id)
    }

    render() {
        return (
            <div className='Quiz'>
                <div className='QuizWrapper'>
                    <h1>
                        Ответьте на все вопросы
                    </h1>

                    {
                        this.state.isFinished
                        ? <FinishedQuiz results={this.state.results}
                                        quiz={this.state.quiz}
                                        onRetry={this.retryHandler}
                            /> :
                        <ActiveQuiz
                            question={this.state.quiz[this.state.activeQuestion].question}
                            answers={this.state.quiz[this.state.activeQuestion].answers}
                            quizLength={this.state.quiz.length}
                            onAnswerClick = {this.onAnswerClickHandler}
                            answerNumber = {this.state.activeQuestion + 1}
                            state={this.state.answerState}
                        />
                    }


                </div>
            </div>
        )
    }
}
export default Quiz;

