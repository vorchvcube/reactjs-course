import React from  'react'
import './AnswerList.scss'
import AnswerItem from './AnswerItem/AnswerItem'

const AnswerList = props => (
    <ul className='AnswersList'>
        {props.answers.map((answer, index)=> {
            return (
                <AnswerItem
                    key={index}
                    state = {props.state ? props.state[answer.id] : null}
                    answer = {answer}
                    onAnswerClick={props.onAnswerClick}>
                </AnswerItem>
            )
        })}
    </ul>
);


export default AnswerList;

